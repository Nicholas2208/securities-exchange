package com.example.subpayment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SubPaymentApplication {

    public static void main(String[] args) {
        SpringApplication.run(SubPaymentApplication.class, args);
    }

}
